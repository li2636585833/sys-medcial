package com.kgc.mapper;

import com.kgc.entity.SysRegistrationForm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 挂号单表 Mapper 接口
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface SysRegistrationFormMapper extends BaseMapper<SysRegistrationForm> {

}
