package com.kgc.mapper;

import com.kgc.entity.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 就诊科室表 Mapper 接口
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
