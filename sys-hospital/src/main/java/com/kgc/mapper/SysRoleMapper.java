package com.kgc.mapper;

import com.kgc.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
