package com.kgc.mapper;

import com.kgc.entity.SysRoom;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 就诊房间表 Mapper 接口
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface SysRoomMapper extends BaseMapper<SysRoom> {

}
