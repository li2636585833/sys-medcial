package com.kgc.mapper;

import com.kgc.entity.SysPatient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 病人信息表 Mapper 接口
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface SysPatientMapper extends BaseMapper<SysPatient> {

}
