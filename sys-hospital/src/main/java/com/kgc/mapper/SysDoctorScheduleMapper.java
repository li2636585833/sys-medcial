package com.kgc.mapper;

import com.kgc.entity.SysDoctorSchedule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医生排班表 Mapper 接口
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface SysDoctorScheduleMapper extends BaseMapper<SysDoctorSchedule> {

}
