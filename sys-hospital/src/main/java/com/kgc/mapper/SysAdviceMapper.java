package com.kgc.mapper;

import com.kgc.entity.SysAdvice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医嘱表 Mapper 接口
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface SysAdviceMapper extends BaseMapper<SysAdvice> {

}
