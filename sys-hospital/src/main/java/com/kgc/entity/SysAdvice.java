package com.kgc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 医嘱表
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@ApiModel(value="SysAdvice对象", description="医嘱表")
public class SysAdvice implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "医嘱id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "挂号单id")
    private Integer registrationFormId;

    @ApiModelProperty(value = "医生id")
    private Integer doctorId;

    @ApiModelProperty(value = "病人主诉")
    private String patientComplains;

    @ApiModelProperty(value = "就诊情况")
    private String diagnosisSituation;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getRegistrationFormId() {
        return registrationFormId;
    }

    public void setRegistrationFormId(Integer registrationFormId) {
        this.registrationFormId = registrationFormId;
    }
    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }
    public String getPatientComplains() {
        return patientComplains;
    }

    public void setPatientComplains(String patientComplains) {
        this.patientComplains = patientComplains;
    }
    public String getDiagnosisSituation() {
        return diagnosisSituation;
    }

    public void setDiagnosisSituation(String diagnosisSituation) {
        this.diagnosisSituation = diagnosisSituation;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "SysAdvice{" +
            "id=" + id +
            ", registrationFormId=" + registrationFormId +
            ", doctorId=" + doctorId +
            ", patientComplains=" + patientComplains +
            ", diagnosisSituation=" + diagnosisSituation +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
        "}";
    }
}
