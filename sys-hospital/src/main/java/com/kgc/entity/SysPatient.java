package com.kgc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 病人信息表
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@ApiModel(value="SysPatient对象", description="病人信息表")
public class SysPatient implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "病人id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "生日")
    private LocalDateTime birth;

    @ApiModelProperty(value = "性别")
    private String sex;

    @ApiModelProperty(value = "住址")
    private String address;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "身份证号")
    private Long idCard;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否有医保（0无1有）")
    private String medicalInsurance;

    @ApiModelProperty(value = "医保卡号")
    private Long medicalInsuranceId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public LocalDateTime getBirth() {
        return birth;
    }

    public void setBirth(LocalDateTime birth) {
        this.birth = birth;
    }
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public Long getIdCard() {
        return idCard;
    }

    public void setIdCard(Long idCard) {
        this.idCard = idCard;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public String getMedicalInsurance() {
        return medicalInsurance;
    }

    public void setMedicalInsurance(String medicalInsurance) {
        this.medicalInsurance = medicalInsurance;
    }
    public Long getMedicalInsuranceId() {
        return medicalInsuranceId;
    }

    public void setMedicalInsuranceId(Long medicalInsuranceId) {
        this.medicalInsuranceId = medicalInsuranceId;
    }

    @Override
    public String toString() {
        return "SysPatient{" +
            "id=" + id +
            ", name=" + name +
            ", birth=" + birth +
            ", sex=" + sex +
            ", address=" + address +
            ", phone=" + phone +
            ", idCard=" + idCard +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", medicalInsurance=" + medicalInsurance +
            ", medicalInsuranceId=" + medicalInsuranceId +
        "}";
    }
}
