package com.kgc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 挂号单表
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@ApiModel(value="SysRegistrationForm对象", description="挂号单表")
public class SysRegistrationForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "挂号单id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "病人id")
    private Integer patientId;

    @ApiModelProperty(value = "排班表id")
    private Integer scheduleId;

    @ApiModelProperty(value = "挂号人id")
    private Integer userId;

    @ApiModelProperty(value = "就诊科室id")
    private Integer deptId;

    @ApiModelProperty(value = "就诊房间id")
    private Integer roomId;

    @ApiModelProperty(value = "挂号状态（1已挂号未问诊2正问诊3问诊结束）")
    private String registrationStatus;

    @ApiModelProperty(value = "挂号费")
    private String registrationFee;

    @ApiModelProperty(value = "实际支付挂号费")
    private String actualPayment;

    @ApiModelProperty(value = "医保报销费用")
    private String medicare;

    @ApiModelProperty(value = "是否使用医保（0否1是）")
    private String medicalInsurance;

    @ApiModelProperty(value = "挂号时间")
    private LocalDateTime registrationTime;

    @ApiModelProperty(value = "医生问诊时间")
    private LocalDateTime doctorConsultationTime;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }
    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }
    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }
    public String getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(String registrationStatus) {
        this.registrationStatus = registrationStatus;
    }
    public String getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(String registrationFee) {
        this.registrationFee = registrationFee;
    }
    public String getActualPayment() {
        return actualPayment;
    }

    public void setActualPayment(String actualPayment) {
        this.actualPayment = actualPayment;
    }
    public String getMedicare() {
        return medicare;
    }

    public void setMedicare(String medicare) {
        this.medicare = medicare;
    }
    public String getMedicalInsurance() {
        return medicalInsurance;
    }

    public void setMedicalInsurance(String medicalInsurance) {
        this.medicalInsurance = medicalInsurance;
    }
    public LocalDateTime getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(LocalDateTime registrationTime) {
        this.registrationTime = registrationTime;
    }
    public LocalDateTime getDoctorConsultationTime() {
        return doctorConsultationTime;
    }

    public void setDoctorConsultationTime(LocalDateTime doctorConsultationTime) {
        this.doctorConsultationTime = doctorConsultationTime;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "SysRegistrationForm{" +
            "id=" + id +
            ", patientId=" + patientId +
            ", scheduleId=" + scheduleId +
            ", userId=" + userId +
            ", deptId=" + deptId +
            ", roomId=" + roomId +
            ", registrationStatus=" + registrationStatus +
            ", registrationFee=" + registrationFee +
            ", actualPayment=" + actualPayment +
            ", medicare=" + medicare +
            ", medicalInsurance=" + medicalInsurance +
            ", registrationTime=" + registrationTime +
            ", doctorConsultationTime=" + doctorConsultationTime +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
        "}";
    }
}
