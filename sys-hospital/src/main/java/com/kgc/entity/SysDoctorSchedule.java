package com.kgc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 医生排班表
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@ApiModel(value="SysDoctorSchedule对象", description="医生排班表")
public class SysDoctorSchedule implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "医生排班表id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "医生id")
    private Integer userId;

    @ApiModelProperty(value = "科室id")
    private Integer deptId;

    @ApiModelProperty(value = "房间id")
    private Integer roomId;

    @ApiModelProperty(value = "值班时间（1上午2下午）")
    private String attendedTime;

    @ApiModelProperty(value = "诊断病人数量")
    private String patientsDiagnosedNum;

    @ApiModelProperty(value = "剩余病人数量")
    private String patientsRemainingNum;

    @ApiModelProperty(value = "问诊时间")
    private LocalDateTime consultationTime;

    @ApiModelProperty(value = "删除标识（0否1是）")
    private String removeIdentity;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }
    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }
    public String getAttendedTime() {
        return attendedTime;
    }

    public void setAttendedTime(String attendedTime) {
        this.attendedTime = attendedTime;
    }
    public String getPatientsDiagnosedNum() {
        return patientsDiagnosedNum;
    }

    public void setPatientsDiagnosedNum(String patientsDiagnosedNum) {
        this.patientsDiagnosedNum = patientsDiagnosedNum;
    }
    public String getPatientsRemainingNum() {
        return patientsRemainingNum;
    }

    public void setPatientsRemainingNum(String patientsRemainingNum) {
        this.patientsRemainingNum = patientsRemainingNum;
    }
    public LocalDateTime getConsultationTime() {
        return consultationTime;
    }

    public void setConsultationTime(LocalDateTime consultationTime) {
        this.consultationTime = consultationTime;
    }
    public String getRemoveIdentity() {
        return removeIdentity;
    }

    public void setRemoveIdentity(String removeIdentity) {
        this.removeIdentity = removeIdentity;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "SysDoctorSchedule{" +
            "id=" + id +
            ", userId=" + userId +
            ", deptId=" + deptId +
            ", roomId=" + roomId +
            ", attendedTime=" + attendedTime +
            ", patientsDiagnosedNum=" + patientsDiagnosedNum +
            ", patientsRemainingNum=" + patientsRemainingNum +
            ", consultationTime=" + consultationTime +
            ", removeIdentity=" + removeIdentity +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
        "}";
    }
}
