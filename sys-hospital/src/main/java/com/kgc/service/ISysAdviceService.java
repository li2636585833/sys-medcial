package com.kgc.service;

import com.kgc.entity.SysAdvice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医嘱表 服务类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface ISysAdviceService extends IService<SysAdvice> {

}
