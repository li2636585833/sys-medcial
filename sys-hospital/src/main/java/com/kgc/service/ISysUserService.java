package com.kgc.service;

import com.kgc.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface ISysUserService extends IService<SysUser> {

}
