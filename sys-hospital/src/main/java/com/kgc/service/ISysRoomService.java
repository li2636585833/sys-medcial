package com.kgc.service;

import com.kgc.entity.SysRoom;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 就诊房间表 服务类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface ISysRoomService extends IService<SysRoom> {

}
