package com.kgc.service.impl;

import com.kgc.entity.SysRegistrationForm;
import com.kgc.mapper.SysRegistrationFormMapper;
import com.kgc.service.ISysRegistrationFormService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 挂号单表 服务实现类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@Service
public class SysRegistrationFormServiceImpl extends ServiceImpl<SysRegistrationFormMapper, SysRegistrationForm> implements ISysRegistrationFormService {

}
