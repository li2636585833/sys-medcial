package com.kgc.service.impl;

import com.kgc.entity.SysAdvice;
import com.kgc.mapper.SysAdviceMapper;
import com.kgc.service.ISysAdviceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医嘱表 服务实现类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@Service
public class SysAdviceServiceImpl extends ServiceImpl<SysAdviceMapper, SysAdvice> implements ISysAdviceService {

}
