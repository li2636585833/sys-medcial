package com.kgc.service.impl;

import com.kgc.entity.SysRole;
import com.kgc.mapper.SysRoleMapper;
import com.kgc.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
