package com.kgc.service.impl;

import com.kgc.entity.SysDoctorSchedule;
import com.kgc.mapper.SysDoctorScheduleMapper;
import com.kgc.service.ISysDoctorScheduleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医生排班表 服务实现类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@Service
public class SysDoctorScheduleServiceImpl extends ServiceImpl<SysDoctorScheduleMapper, SysDoctorSchedule> implements ISysDoctorScheduleService {

}
