package com.kgc.service.impl;

import com.kgc.entity.SysRoom;
import com.kgc.mapper.SysRoomMapper;
import com.kgc.service.ISysRoomService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 就诊房间表 服务实现类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@Service
public class SysRoomServiceImpl extends ServiceImpl<SysRoomMapper, SysRoom> implements ISysRoomService {

}
