package com.kgc.service.impl;

import com.kgc.entity.SysPatient;
import com.kgc.mapper.SysPatientMapper;
import com.kgc.service.ISysPatientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 病人信息表 服务实现类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@Service
public class SysPatientServiceImpl extends ServiceImpl<SysPatientMapper, SysPatient> implements ISysPatientService {

}
