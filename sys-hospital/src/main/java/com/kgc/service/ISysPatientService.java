package com.kgc.service;

import com.kgc.entity.SysPatient;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 病人信息表 服务类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface ISysPatientService extends IService<SysPatient> {

}
