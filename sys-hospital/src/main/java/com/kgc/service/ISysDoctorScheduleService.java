package com.kgc.service;

import com.kgc.entity.SysDoctorSchedule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医生排班表 服务类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface ISysDoctorScheduleService extends IService<SysDoctorSchedule> {

}
