package com.kgc.service;

import com.kgc.entity.SysRegistrationForm;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 挂号单表 服务类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface ISysRegistrationFormService extends IService<SysRegistrationForm> {

}
