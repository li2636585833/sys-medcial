package com.kgc.service;

import com.kgc.entity.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 就诊科室表 服务类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface ISysDeptService extends IService<SysDept> {

}
