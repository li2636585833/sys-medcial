package com.kgc.service;

import com.kgc.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
public interface ISysRoleService extends IService<SysRole> {

}
