package com.kgc.controller;


import java.util.List;

import org.apache.ibatis.javassist.expr.NewArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.kgc.common.result.Result;
import com.kgc.entity.SysRegistrationForm;
import com.kgc.service.ISysRegistrationFormService;

/**
 * <p>
 * 挂号单表 前端控制器
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@RestController
@RequestMapping("/sys-registration-form")
public class SysRegistrationFormController {
	
	@Autowired
	private ISysRegistrationFormService registrationFormService;
	
	@RequestMapping("/getRegistrationForm")
	public Object getRegistrationForm() {
		List<SysRegistrationForm> list = registrationFormService.list();
		return new Result("0","",list,list.size());
	}

}
