package com.kgc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 就诊科室表 前端控制器
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@RestController
@RequestMapping("/sys-dept")
public class SysDeptController {

}
