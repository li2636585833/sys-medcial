package com.kgc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.kgc.service.ISysPatientService;

/**
 * <p>
 * 病人信息表 前端控制器
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@RestController
@RequestMapping("/sys-patient")
public class SysPatientController {
	@Autowired
	ISysPatientService patientService;
	
	@RequestMapping("/patientList")
	public Object patientList() {
		return patientService.list();
	}
}
