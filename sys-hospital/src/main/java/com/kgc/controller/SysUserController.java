package com.kgc.controller;


import java.util.List;

import org.apache.ibatis.javassist.expr.NewArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.kgc.common.result.Result;
import com.kgc.entity.SysUser;
import com.kgc.service.ISysUserService;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author libotao
 * @since 2023-03-15
 */
@RestController
@RequestMapping("/sys-users")
public class SysUserController {
	
	@Autowired
	private ISysUserService userService;
	
	@RequestMapping("/getUserList")
	public Object userList() {
		List<SysUser> list = userService.list();
		return new Result("0","",list,list.size());
	}
	
	@RequestMapping("/userList2")
	public Object userList2() {
		
		return userService.list();
	}
}
